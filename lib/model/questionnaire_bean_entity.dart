import 'package:question_module/generated/json/base/json_convert_content.dart';

class QuestionnaireBeanEntity with JsonConvert<QuestionnaireBeanEntity> {
	int success;
	String msg;
	QuestionnaireBeanData data;
	int total;
	int size;
	int current;
}

class QuestionnaireBeanData with JsonConvert<QuestionnaireBeanData> {
	String id;
	String title;
	String description;
	String createTime;
	List<QuestionnaireBeanDataQueston> questons;
	List<int> selected = List();
}

class QuestionnaireBeanDataQueston with JsonConvert<QuestionnaireBeanDataQueston> {
	String id;
	String title;
	bool required;
	int type;
	int sort;
	String answer;
	String contentType;
	List<QuestionnaireBeanDataQuestonsOption> options;
	List<int> selected = List();
}

class QuestionnaireBeanDataQuestonsOption with JsonConvert<QuestionnaireBeanDataQuestonsOption> {
	String id;
	String label;
	String value;
}
