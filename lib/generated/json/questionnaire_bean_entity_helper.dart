import 'package:question_module/model/questionnaire_bean_entity.dart';

questionnaireBeanEntityFromJson(QuestionnaireBeanEntity data, Map<String, dynamic> json) {
	if (json['success'] != null) {
		data.success = json['success']?.toInt();
	}
	if (json['msg'] != null) {
		data.msg = json['msg']?.toString();
	}
	if (json['data'] != null) {
		data.data = new QuestionnaireBeanData().fromJson(json['data']);
	}
	if (json['total'] != null) {
		data.total = json['total']?.toInt();
	}
	if (json['size'] != null) {
		data.size = json['size']?.toInt();
	}
	if (json['current'] != null) {
		data.current = json['current']?.toInt();
	}
	return data;
}

Map<String, dynamic> questionnaireBeanEntityToJson(QuestionnaireBeanEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['success'] = entity.success;
	data['msg'] = entity.msg;
	if (entity.data != null) {
		data['data'] = entity.data.toJson();
	}
	data['total'] = entity.total;
	data['size'] = entity.size;
	data['current'] = entity.current;
	return data;
}

questionnaireBeanDataFromJson(QuestionnaireBeanData data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['description'] != null) {
		data.description = json['description']?.toString();
	}
	if (json['createTime'] != null) {
		data.createTime = json['createTime']?.toString();
	}
	if (json['questons'] != null) {
		data.questons = new List<QuestionnaireBeanDataQueston>();
		(json['questons'] as List).forEach((v) {
			data.questons.add(new QuestionnaireBeanDataQueston().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> questionnaireBeanDataToJson(QuestionnaireBeanData entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['title'] = entity.title;
	data['description'] = entity.description;
	data['createTime'] = entity.createTime;
	if (entity.questons != null) {
		data['questons'] =  entity.questons.map((v) => v.toJson()).toList();
	}
	return data;
}

questionnaireBeanDataQuestonFromJson(QuestionnaireBeanDataQueston data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['title'] != null) {
		data.title = json['title']?.toString();
	}
	if (json['required'] != null) {
		data.required = json['required'];
	}
	if (json['type'] != null) {
		data.type = json['type']?.toInt();
	}
	if (json['sort'] != null) {
		data.sort = json['sort']?.toInt();
	}
	if (json['answer'] != null) {
		data.answer = json['answer']?.toString();
	}
	if (json['contentType'] != null) {
		data.contentType = json['contentType']?.toString();
	}
	if (json['options'] != null) {
		data.options = new List<QuestionnaireBeanDataQuestonsOption>();
		(json['options'] as List).forEach((v) {
			data.options.add(new QuestionnaireBeanDataQuestonsOption().fromJson(v));
		});
	}
	return data;
}

Map<String, dynamic> questionnaireBeanDataQuestonToJson(QuestionnaireBeanDataQueston entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['title'] = entity.title;
	data['required'] = entity.required;
	data['type'] = entity.type;
	data['sort'] = entity.sort;
	data['answer'] = entity.answer;
	data['contentType'] = entity.contentType;
	if (entity.options != null) {
		data['options'] =  entity.options.map((v) => v.toJson()).toList();
	}
	return data;
}

questionnaireBeanDataQuestonsOptionFromJson(QuestionnaireBeanDataQuestonsOption data, Map<String, dynamic> json) {
	if (json['id'] != null) {
		data.id = json['id']?.toString();
	}
	if (json['label'] != null) {
		data.label = json['label']?.toString();
	}
	if (json['value'] != null) {
		data.value = json['value']?.toString();
	}
	return data;
}

Map<String, dynamic> questionnaireBeanDataQuestonsOptionToJson(QuestionnaireBeanDataQuestonsOption entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['id'] = entity.id;
	data['label'] = entity.label;
	data['value'] = entity.value;
	return data;
}