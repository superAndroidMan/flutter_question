
import 'package:oktoast/oktoast.dart';
import 'package:flutter/material.dart';
import 'package:question_module/page/QuestionPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OKToast( // 这一步
      child: new MaterialApp(
        title: 'question_modle',
        theme: new ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: new QuestionPage(),
        routes: <String, WidgetBuilder> {
          'questionPage': (BuildContext context) => new QuestionPage(),
        },
      ),
    );
  }
}
