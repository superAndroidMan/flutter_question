import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:question_module/model/questionnaire_bean_entity.dart';
import 'package:question_module/net/dio_utils.dart';
import 'package:question_module/res/colors.dart';
import 'package:question_module/res/dimens.dart';
import 'package:question_module/res/gaps.dart';
import 'package:question_module/util/log_utils.dart';
import 'package:question_module/util/toast.dart';
import 'package:question_module/widget/app_bar.dart';
import 'package:question_module/widget/load_image.dart';
import 'package:question_module/widget/my_scroll_view.dart';

class QuestionPage extends StatefulWidget {
  @override
  _QuestionPage createState() => _QuestionPage();
}

class _QuestionPage extends State<QuestionPage> {
  List<QuestionnaireBeanDataQueston> mList = List();
  QuestionnaireBeanData data;

  @override
  Widget build(BuildContext context) {
    bool isEmpty = data == null || data.title.length == 0;
    List<Widget> children = [
      Padding(
        padding: new EdgeInsets.only(bottom: 30),
        child: Text(
          isEmpty ? "问卷调查" : data.title,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colours.font_title,
              fontSize: 18,
              fontWeight: FontWeight.bold),
        ),
      ),
      ListView.builder(
        // 如果滚动视图在滚动方向无界约束，那么shrinkWrap必须为true
        shrinkWrap: true,
        // 禁用ListView滑动，使用外层的ScrollView滑动
        physics: const NeverScrollableScrollPhysics(),
        itemCount: mList.length,
        itemBuilder: (_, index) => _getQuestionItem(index),
      ),
      Container(
        height: 44,
        margin: const EdgeInsets.only(left: 20, top: 15, right: 20, bottom: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: FlatButton(
                color: Colors.white,
                textColor: Colours.main_color,
                shape: StadiumBorder(
                  side: BorderSide(
                    color: Colours.main_color,
                    style: BorderStyle.solid,
                  ),
                ),
                child: const Text(
                  '重填',
                  style: TextStyle(fontSize: Dimens.font_sp18),
                ),
                onPressed: () {
                  mList.forEach((element) {
                    element.selected.clear();
                    setState(() {});
                  });
                },
              ),
            ),
            Gaps.hGap16,
            Expanded(
              flex: 1,
              child: FlatButton(
                color: Colours.main_color,
                textColor: Colors.white,
                shape: StadiumBorder(
                  side: BorderSide(
                    color: Colours.main_color,
                    style: BorderStyle.solid,
                  ),
                ),
                child: const Text(
                  '下一步',
                  style: TextStyle(fontSize: Dimens.font_sp18),
                ),
                onPressed: () {
                  if (mList.isNotEmpty) {
                    for (var element in mList) {
                      List<int> list = element.selected;
                      if (list.isEmpty) {
                        Toast.show(element.title + "未选择");
                        break;
                      }
                    }
                  }
                  uploadAnswer();
                },
              ),
            )
          ],
        ),
      ),
    ];

    return Scaffold(
        appBar: MyAppBar(
          centerTitle: '',
          isBack: false,
        ),
        body: MyScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          crossAxisAlignment: CrossAxisAlignment.center,
          children: children,
        ));
  }

  var token = "";
  EventChannel eventChannel = EventChannel('android/params');
  MethodChannel platform = const MethodChannel("com.android.method.channel");
  BasicMessageChannel _messageChannel =
      BasicMessageChannel('com.ios.message.channel', StandardMessageCodec());

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      eventChannel.receiveBroadcastStream().listen((_receiveNativeParams));
    } else {
      _messageChannel.setMessageHandler(_handleIOSMessage);
    }
  }

  void _receiveNativeParams(event) {
    setState(() {
      token = event.toString();
      Map<String, dynamic> mapToken = Map();
      mapToken["token"] = this.token;
      DioUtils.instance.dio.options.headers = mapToken;
      requestDataAndReload();
    });
  }

  Future _handleIOSMessage(message) async {
    String token = message.toString(); //
    Map<String, dynamic> map = Map();
    this.token = token;
    map["token"] = this.token;
    DioUtils.instance.dio.options.headers = map;
    requestDataAndReload();
  }

  Widget _getQuestionItem(int index) {
    QuestionnaireBeanDataQueston bean = mList[index];
    List<QuestionnaireBeanDataQuestonsOption> list = bean.options;
    List<int> listSelect = bean.selected;
    bool isMore = bean.type == 1;
    List<Widget> children = [
      Padding(
        padding: new EdgeInsets.only(left: 7, right: 7, bottom: 15),
        child: RichText(
          text: TextSpan(
              text: bean.title,
              style: TextStyle(
                  color: Colours.font_title,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                  text: isMore ? "[可多选]" : "",
                  style: TextStyle(
                      color: Colours.red,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                )
              ]),
        ),
      ),
      ListView.builder(
        // 如果滚动视图在滚动方向无界约束，那么shrinkWrap必须为true
        shrinkWrap: true,
        // 禁用ListView滑动，使用外层的ScrollView滑动
        physics: const NeverScrollableScrollPhysics(),
        itemCount: list.length,
        padding: new EdgeInsets.only(left: 13, right: 13),
        itemBuilder: (_, index) =>
            _getQuestionTextItem(list[index].label, listSelect, isMore, index),
      )
    ];
    return MergeSemantics(
      child: Container(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: children)),
    );
  }

  //选项item
  Widget _getQuestionTextItem(
      String text, List<int> listSelect, bool isMore, int index) {
    const imageSize = 15.0;
    return MergeSemantics(
      child: Container(
      margin: const EdgeInsets.only(bottom: 15.0),
        child: GestureDetector( behavior: HitTestBehavior.opaque,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(right: 15.0),
                child: listSelect.contains(index)
                    ? const LoadAssetImage('ic_check_true',
                        width: imageSize, height: imageSize)
                    : const LoadAssetImage('icon_check_false',
                        width: imageSize, height: imageSize),
              ),
              Text(
                text,
                style: TextStyle(
                    color: Colours.font_desc, fontWeight: FontWeight.normal),
              ),
            ],
          ),
          onTap: () {
            if (!isMore) {
              listSelect.clear();
            }
            if (listSelect.contains(index)) {
              listSelect.remove(index);
            } else {
              listSelect.add(index);
            }
            setState(() {});
          },
        ),
      ),
    );
  }

  void requestDataAndReload() {
    String path = "/questionnaire/paper";
    DioUtils.instance.asyncRequestNetworkString<QuestionnaireBeanData>(
      Method.get,
      path,
      onSuccess: (result) {
        final responseData = json.decode(result);
        var bean = new QuestionnaireBeanEntity().fromJson(responseData);
        setState(() {
          mList = bean.data.questons;
          data = bean.data;
        });
      },
    );
  }

  void uploadAnswer() async {
    String path = "/questionnaire/answer";
    Map<String, dynamic> map = Map<String, dynamic>();
    List<Map> list = List();
    map["paperId"] = data.id;
    mList.forEach((element) {
      Map<String, dynamic> mapOption = Map<String, dynamic>();
      List<int> select = element.selected;
      List<String> optionIds = List();
      List<String> optionAnswer = List();
      select.forEach((index) {
        optionIds.add(element.options[index].id);
        String value = element.options[index].value;
        String label = element.options[index].label;
        bool isEmpty = value == null || value.length == 0;
        optionAnswer.add(isEmpty ? label : value);
      });
      mapOption["id"] = element.id;
      mapOption["answers"] = optionAnswer;
      mapOption["label"] = element.title;
      mapOption["optionIds"] = optionIds;
      list.add(mapOption);
    });
    map["answers"] = list;

    String text = json.encode(map);

    ///创建Dio
    // Options _options = Options(extra: map);
    DioUtils.instance.asyncRequestNetworkString(
      Method.post,
      path,
      params: text,
      onSuccess: (result) {
        final responseData = json.decode(result);
        Map<String, dynamic> data = responseData;
        Toast.show(data["msg"]);
        if (data["success"] == 1000) {
          platform.invokeMethod("send_finish");
          SystemNavigator.pop();
        }
      },
    );
  }
}
