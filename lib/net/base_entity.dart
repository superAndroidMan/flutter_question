

import 'package:question_module/generated/json/base/json_convert_content.dart';

import '../Constant.dart';

class BaseEntity<T> {

  int success;
  String msg;
  T data;
  List<T> listData = [];

  BaseEntity(this.success, this.msg, this.data);

  BaseEntity.fromJson(Map<String, dynamic> json) {
    success = json[Constant.code] as int;
    msg = json[Constant.message] as String;
    if (json.containsKey(Constant.data)) {
      if (json[Constant.data] is List) {
        json[Constant.data].forEach((Object item) {
          listData.add(_generateOBJ<T>(item));
        });
      } else {
        data = _generateOBJ(json[Constant.data]);
      }
    }
  }

  S _generateOBJ<S>(Object json) {
    if (S.toString() == 'String') {
      return json.toString() as S;
    } else if (T.toString() == 'Map<dynamic, dynamic>') {
      return json as S;
    } else {
      return JsonConvert.fromJsonAsT<S>(json);
    }
  }
}