import 'package:question_module/model/Questionbean.dart';

class DataUtil {
  static List<QuestionBean> getData() {

    List<QuestionBean> mList = new List();

    List<String> list1 = new List();
    list1.add("没有");
    list1.add("偶尔");
    list1.add("经常");

    List<String> list2 = new List();
    list2.add("没有");
    list2.add("有");

    List<String> list3 = new List();
    list3.add("漱口水");
    list3.add("牙线");
    list3.add("电动牙刷");
    list3.add("水牙线");
    list3.add("家用洁牙仪");
    list3.add("其他");

    List<String> list4 = new List();
    list4.add("美白");
    list4.add("去渍");
    list4.add("去牙结石");
    list4.add("敏感护理");
    list4.add("牙龈护理");

    mList.add(new QuestionBean("1、您平时食用冷热酸甜食物时，是否存在牙齿敏感:", list1,false));
    mList.add(new QuestionBean("2、您平时刷牙时，是否有牙龈出血的情况:", list1,false));
    mList.add(new QuestionBean("3、您平时是否在进食后有牙龈疼痛、出血的情况:", list1,false));
    mList.add(new QuestionBean("4、您是否会口腔溃疡:", list1,false));
    mList.add(new QuestionBean("5、您是否有洁牙习惯:", list2,false));
    mList.add(new QuestionBean("6、除牙刷、牙膏外，您平时还是用哪些口腔护理用品:", list3,true));
    mList.add(new QuestionBean("7、您近期有哪些口腔护理需求:", list4,true));

    return mList;
  }
}
